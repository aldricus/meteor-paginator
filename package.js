// Do not parse this file if loading the app!
//if(typeof Meteor !== 'undefined') return;

Package.describe({
  name: 'aldricus:paginator',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Create a pagination with a given  Meteor collection',
  git: 'https://bitbucket.org/aldricus/meteor-paginator',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');
  api.use('jquery');
  api.addFiles('server/server_pagination.js', 'server');
  api.addFiles('client/client_pagination.js', 'client');
  api.addFiles('client/paginate.css', 'client');
  //api.export('ServerPaginator');
});

Package.onTest(function(api) {
  api.use('jquery');
  api.use('tinytest');
  api.use('aldricus:paginator');
  //add test files in tests directory in order to exclude them from  Meteor app
  //api.addAssets('tests/tinytest/pagination_test.js');
});
