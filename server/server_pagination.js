/*
    Meteor Pagination - Copyright (C) 2015  Aldric T
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

var Paginator = function(name, perPage, collection, fields) {
    this.callback = function() {};
    this.fields = fields;
    this.data = {};
    this.searchTerms = {};
    this.skip = 0;
    this.perPage = perPage;
    this.collection = collection;
    this.name = name;
    return this;
};

Paginator.prototype.init = function() {

    this.total = this.count();
    this.pageCnt = this.pageCount();
    this.lastPage = this.pageCnt;
    this.currentPage = 1;
    this.from = 0;
    this.to = 0;
    return this;
};

Paginator.prototype.details = function() {
    return {
        'total': this.total,
        'pageCount': this.pageCnt,
        'currentPage': this.currentPage,
        'perPage': this.perPage,
        'from': this.from,
        'lastPage': this.lastPage
    };
};

Paginator.prototype.count = function() {
    if (this.searchTerms === undefined) {
        this.searchTerms = {};
    }
    // Hack to convert id to Mongodb Ids
    if (this.searchTerms._id) {
        var arr = this.searchTerms._id.$in;
        this.searchTerms._id.$in = _.map(arr, function(id) {
            var objID;
            try {
                objID = new MongoInternals.NpmModule.ObjectID(id);
            } catch (err) {
                console.log(err, id);
            }
            return objID;
        });
    }
    console.log('server_pagination this.searchTerms: ', this.searchTerms);
    return this.collection.find(
        this.searchTerms, {
            limit: this.perPage,
            skip: this.skip,
            fields: this.fields
        }).count();
};

Paginator.prototype.find = function(currentPage) {
    var self = this;
    self.currentPage = currentPage;
    self.skip = self.perPage * (currentPage - 1);
    self.from = self.skip;

    if (self.searchTerms === undefined) {
        self.searchTerms = {};
    }
    var f = self.collection.find(
        self.searchTerms, {
            sort: self.data.sortBy,
            skip: self.skip,
            limit: self.perPage,
            fields: self.fields
        });
    var a = f;
    return f;
};

Paginator.prototype.pageCount = function() {
    var n = Math.floor(this.total / this.perPage);
    var mod = (this.total % this.perPage !== 0) ? 1 : 0;
    return n + mod;
};

Paginator.prototype.customMongoQuery = function() {};

Paginator.prototype.load = function(callback, customMongoQuery) {
    this.callback = callback;
    var self = this;

    Meteor.publish(self.name, function(currentPage, data) {

        var that = this;
        var validator = {
            nb: Match.Optional(Match.Integer), //can be undefined
            sortBy: Match.OneOf([String], Object, undefined),
            terms: Match.OneOf(Object, [Match.Any], String, [String], null, undefined),
            path: Match.Optional(String), // can be undefined
            displayTotal: Match.Optional(Boolean) // can be undefined
        };

        if (data.nb === undefined) {
            validator = {
                sortBy: Match.OneOf([String], Object, undefined),
                terms: Match.OneOf(Object, [Match.Any], String, [String], null),
            };
        }

        check(currentPage, Match.Integer);
        check(data, validator);
        self.data = data;
        if (_.isFunction(customMongoQuery)) {
            self.customMongoQuery = customMongoQuery;
            var results = self.callback(self.data.terms, self).customMongoQuery(self, currentPage);
            var objectId;
            if (_.isArray(results)) {
                _(results).each(function(result) {
                    that.added(self.collection._name, result._id.toString(), result);
                });
                that.ready();
            }
        } else {
            return self.callback(self.data.terms, self).find(currentPage);
        }
    });
};

Paginator.prototype.getSearchTerms = function(callback) {
    return callback(this.searchTerms, this);
};

Paginator.prototype.responseCallback = function(searchData) {
    this.searchTerms = searchData;
    return this.init();
};

// Must be called only once
Meteor.ServerPaginator = function() {
    this.ldrcPaginators = [];
    var self = this;
    Meteor.methods({
        // Method used to get the pagination
        // details such as page_count,
        // per_page, total, etc
        ldrcPaginator: function(name) {
            check(name, String);
            var d = self.ldrcPaginators[name].details();
            return d;
        }
    });
    return this;
};

Meteor.ServerPaginator.prototype.addPagination = function(name, collection, fields, perPage, callback, customMongoQueryCallback) {

    if (name === undefined || collection === undefined) {
        throw new Error('A name and a collection must be provided: Meteor.paginate(<name>, <.Mongo.Collection>, <options>)');
    }
    this.ldrcPaginators[name] = new Paginator(name, perPage, collection, fields);
    this.ldrcPaginators[name].load(callback, customMongoQueryCallback);
    return this;
};