/*
    Meteor Pagination - Copyright (C) 2015  Aldric T
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
Meteor.ClientPaginator = function(nb) {
    if (nb === undefined) {
        nb = 8;
    }
    this.arrWidgets = [];
    this.name = null;
    this.currentPage = 1;
    this.defaultOpts = {};
    this.defaultOpts.nb = nb;
    this.part = 1;
    this.left = 1;
    this.right = nb;
    this.nb = nb;
    this.dataHandler = {};
};

Meteor.ClientPaginator.prototype.link = function(name, options, dataHandlerCallback) {
    if (dataHandlerCallback === undefined) {
        dataHandlerCallback = function() {};
    }
    var self = this;
    //init user options
    $.each(options, function(key, value) {
        self.defaultOpts[key] = value;
    });
    /**
     * options:
     * {
     * sortBy: <use to sort the collection>,
     * terms: <JSON expression used as a pattern to match value in mongodb. You can
     * use it in ServerPagination to format it and match string in queries>,
     * clickHandler: function(e){},
     * getDetails: function(details, error){}, //get pagination details
     * setTemplate: function(data){
     *     return <TODO: set here custom display of the pagination>;
     *  },
     *  nb: <number of page number to display in paginator>,
     *  path: <used in href>,
     *  displayTotal: false,
     *  collectionChange: function(collection){}
     * }
     */
    var Pagination = function(id) {
        /**
         * Discription
         * Pagination Widget
         */

        /**
         * Paginate namespace: currently not used
         */
        var paginates = [];

        var config;
        /**
         * Main container of pagination block element
         */
        var $elPaginate = $('#' + id);
        /**
         * Callback function on click event
         */
        var onClickCallback;
        /**
         * Render the pagination widget
         */
        var render = function(options) {

            var current_page = parseInt(options.current_page);
            var last_page = parseInt(options.last_page);
            var pageCount = parseInt(options.page_count);
            var move = false;
            if (options.total <= options.per_page) {
                // Skip and hide pagination if total is lower than item per page
                $elPaginate.html('');
                return;
            }

            //set pagination when page has reach the last
            //visible link on the right
            if (self.part > self.defaultOpts.nb) {
                var t = last_page - (current_page - 1);
                if (t < self.defaultOpts.nb) {
                    self.nb = t;
                } else {
                    self.nb = self.defaultOpts.nb;
                }
                self.part = 1;
                move = true;
            }
            //set pagination when page has reach the las
            //visible link on the left
            if (self.part === 0 && current_page >= self.nb) {
                self.part = self.nb = self.defaultOpts.nb;
                self.left = current_page - (self.nb - 1);
                self.right = current_page;
            }
            //set pagination when current page is the last one
            if (current_page == last_page) {
                var mod = last_page % self.defaultOpts.nb;

                self.nb = (mod !== 0) ? mod : self.defaultOpts.nb;
                self.left = current_page - (self.nb - 1);
                self.right = current_page;

            }
            //set pagination when current page is first one
            if (current_page == 1) {
                self.part = 1;
                self.left = 1;
                self.right = self.defaultOpts.nb;
                if (pageCount == -1) {
                    self.right = 1;
                } else {
                    if (self.nb > pageCount) {
                        self.right = pageCount;
                    } else {
                        self.right = self.defaultOpts.nb;
                    }
                }
            }

            if (current_page > self.nb && move) {
                self.left = current_page;
                self.right = current_page + (self.nb - 1);
            }

            var list = '<div class="paginate-container"><ul>';
            var prev = current_page - 1;

            var hrefFirst = 'href="' + options.url + '?page=1"';
            var hrePrev = 'href="' + options.url + '?page=' + prev + '"';
            if (current_page == 1) {
                hrePrev = '';
                hrefFirst = '';
                self.part = 1;
            }
            list += '<li class="navstart arrow-l nav"><div><a ' + hrefFirst
                    + ' ><div class="triangle"></div><div class="triangle"></div><a/></div></li>';
            list += '<li class="navstart arrow-s nav"><div><div class="triangle"><a '
                    + hrePrev + '><a/></div></div></li>';

            var j = 1;
            for (var i = self.left; i <= self.right; i++) {

                if (current_page == i) {
                    list += '<li id="item' + j + '" class="active"><div><a>' + i + '</a></div></li>';
                } else {
                    list += '<li id="item' + j + '"><div><a href="'
                            + options.url + '?page=' + i + options.params
                            + '">' + i + '</a></div></li>';
                }
                if (j == parseInt((self.right - self.left) / 2)) {
                    var total = '',
                        totalClass = '';

                    if (self.defaultOpts.displayTotal) {
                        totalClass = "totalClass";
                        total = '(' + options.from + '/' + options.total + ')';
                    }
                    list += '<li class="page-count nav ' + totalClass +'"><div>'
                            + current_page + '/' + last_page
                            + '<br>' + total + '</div></li>';
                }
                j++;
            }
            var next = current_page + 1;
            var hrefLast = 'href="' + options.url + '?page=' + last_page + '"';
            var hrefNext = 'href="' + options.url + '?page=' + next + '"';

            if (current_page == last_page) {
                hrefNext = '';
                hrefLast = '';
                self.part = self.nb;
            }

            list += '<li class="navend arrow-s nav"><div><div><div class="triangle"><a '
                    + hrefNext + ' ><a/></div></div></li>';
            list += '<li class="navend arrow-l nav"><div><div><a ' + hrefLast
                    + ' ><div class="triangle"></div><div class="triangle"></div><a/></div></li>';

            list += '</ul>';

            if (pageCount > 1) {
                if (options.total > 0) {
                    $elPaginate.html(list);
                } else {
                    $elPaginate.html('<span>Sorry nothing found :(</span>');
                }
            }
            //add event on click listener on each anchor
            var clickHandler = function(e) {
                e.preventDefault();
                var $link = $(this);
                $link.unbind('click');
                var n = $link.prop('id').substr(4);
                if (isNaN(n) || n === '') {
                    if ($link.hasClass('navend')) {
                        self.part++;
                    } else if ($link.hasClass('navstart')) {
                        self.part = self.part - 1;
                    }
                } else {
                    self.part = n;
                }
                onClickCallback($link.find('a')[0]);
                if (self.defaultOpts.clickHandler === undefined) {

                    if (!$link.hasClass('arrow')) {
                        $('html, body').animate({
                            scrollTop: 0
                        }, 'fast');
                    }

                } else {
                    self.defaultOpts.clickHandler(e);
                }
            };
            var $lis = $elPaginate.find('li:not(".active"), li:not(".page-count")');
            $lis.unbind('click', clickHandler);
            $lis.bind('click', clickHandler);

            //remove event from nav button if needed
            if (current_page == 1) {
                $('.navstart').unbind('click', clickHandler);
                $('.navstart').css('cursor', 'auto');
            }

            if (current_page == last_page) {
                $('.navend').unbind('click', clickHandler);
                $('.navend').css('cursor', 'auto');
            }
        };

        var pagination = {

            /**
             * Initialise la pagination
             */
            link: function(options, callback) {
                /**
                Mandatory options,
                example:
                    total: data.total,
                    per_page: data.per_page,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    from: data.from,
                    to: data.to
                    url: url
                    nb: 10 //optionnel, nombre de lien de pagination à afficher
                    params: //optional url query parameter &param1=val1&param2=val2,
                    template: function(data){}
                    */
                if (callback === undefined) {
                    throw new Error('A callback function is required');
                }
                onClickCallback = callback;
                if (options.nb === undefined) {
                    if (options.last_page > 10) {
                        options.nb = 10;
                    } else {
                        options.nb = options.last_page;
                    }
                } else {
                    if (options.last_page < 10) {
                        options.nb = options.last_page;
                    }
                }
                if (options.params === undefined) {
                    options.params = '';
                }

                render(options);

                return this;
            },
            /**
             * Recharge la pagination après un clic sur un lien
             * Par exemple, peut être appelé dans le retour Ajax
             * en cas de succès
             */
            reload: function(currentPage) {
                config.current_page = parseInt(currentPage);
                render(config);

                return this;
            }
        };

        return pagination;
    };

    // We create the widget
    self.arrWidgets[name] = new Pagination(name);
    self.name = name;

    Session.setDefault('ldrcCurrentPage' + self.name, 1);
    Session.set('ldrcData' + self.name, self.defaultOpts);
    Tracker.autorun(function() { //Deps.autorun
        // Session.get is reactive, whenever currentPage value
        // is update with Session.set(...) autorun run again.
        self.dataHandler = Meteor.subscribe(
            self.name,
            Session.get('ldrcCurrentPage' + self.name),
            Session.get('ldrcData' + self.name), {
                onReady: function(err) {
                    Meteor.call('ldrcPaginator', self.name, function(error, result) {
                        console.log(result);
                        self.updatePagination(result);
                        if (self.defaultOpts.getDetails !== undefined) {
                            self.defaultOpts.getDetails(result, error);
                        }
                    });

                },
                onError: function(err) {}
            }
        );
        dataHandlerCallback(self.dataHandler);
    });
};

// Used to reload the pagination once you want to
// refresh the collection using terms to match string
// in queries
Meteor.ClientPaginator.prototype.reload = function(page, searchOptions) {
    if (searchOptions !== undefined) {
        this.defaultOpts.terms = searchOptions.terms;
        this.defaultOpts.sortBy = searchOptions.sortBy;
        Session.set('ldrcData' + this.name, {
            sortBy: this.defaultOpts.sortBy,
            terms: this.defaultOpts.terms
        });
    }
    Session.set('ldrcCurrentPage' + this.name, page);
};

// We get the current page with is set by default
// in the Client Paginator to 1
//var currentPage = Session.get('ldrcCurrentPage');
// Function call when all pagination neeed to be updated
// for example when we doing a search, new patterne, new date, etc
//recharge la pagination - crée une nouvelle pagination
//en fonction du nombre de pages
Meteor.ClientPaginator.prototype.updatePagination = function(data) {
    var self = this;
    this.currentPage = data.currentPage;
    this.arrWidgets[this.name].link({
            total: data.total,
            per_page: data.perPage,
            current_page: data.currentPage,
            last_page: data.lastPage,
            from: data.from,
            page_count: data.pageCount,
            to: data.to,
            url: (self.defaultOpts.path !== undefined ? self.defaultOpts.path : this.name),
            nb: self.defaultOpts.nb
                //params: params
        },
        // Function call when we click on any pagination link
        // Only the current page is refreshed, no need to update all
        // pagination
        function(anchor) {
            if(anchor != undefined){
                var params = anchor.href.substr(anchor.href.indexOf('?') + 1);
                params = params.split('&');
                var page = 0,
                    tmp;
                for (var i = 0; i < params.length; i++) {
                    tmp = params[i].split('=');
                    if ([tmp[0]] == 'page') {
                        page = tmp[1];
                    }
                }
                Session.set('ldrcData' + self.name, {
                    sortBy: self.defaultOpts.sortBy,
                    terms: self.defaultOpts.terms
                });
                Session.set('ldrcCurrentPage' + self.name, parseInt(page));
            }
        });
};